'use strict';

(function (ChartJsProvider) {
  ChartJsProvider.setOptions({ colors : [ '#800000', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });
}); 

/**
 * @ngdoc overview
 * @name threeApp
 * @description
 * # threeApp
 *
 * Main module of the application.
 */
angular
  .module('threeApp', [
    'ui.router', 
    'ngMap',
    'chart.js'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
  var map = {
    name: 'map',
    url: '/',
    templateUrl: 'views/main.html',
    controller: 'MainCtrl',
    controllerAs: 'main'
  }

  var chart = {
    name: 'chart',
    url: '/chart',
    templateUrl: 'views/about.html',
    controller: 'AboutCtrl',
    controllerAs: 'about',
  }

  var orders = {
    name: 'orders',
    url: '/orders',
    templateUrl: 'views/orders.html',
    controller: 'ordersCtrl',
    controllerAs: 'orders',
  }

  $stateProvider.state(map);
  $stateProvider.state(chart);
  $stateProvider.state(orders);
  $urlRouterProvider.otherwise('/'); 
})
  // .config(function ($routeProvider) {
  //   $routeProvider
  //     .when('/', {
  //       templateUrl: 'views/main.html',
  //       controller: 'MainCtrl',
  //       controllerAs: 'main',
  //       activetab: 'map'
  //     })
  //     .when('/chart', {
  //       templateUrl: 'views/about.html',
  //       controller: 'AboutCtrl',
  //       controllerAs: 'about',
  //       activetab: 'chart'
  //     })
  //     .otherwise({
  //       redirectTo: '/'
  //     });
  // })
  // // Optional configuration
  // .config(['ChartJsProvider', function (ChartJsProvider) {
  //   // Configure all charts
  //   ChartJsProvider.setOptions({
  //     chartColors: ['#FF5252', '#FF8A80'],
  //     responsive: false
  //   });
  //   // Configure all line charts
  //   ChartJsProvider.setOptions('line', {
  //     showLines: false
  //   });
  // }])

  ;
