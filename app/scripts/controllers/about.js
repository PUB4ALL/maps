'use strict';

/**
 * @ngdoc function
 * @name threeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the threeApp
 */
angular.module('threeApp')
  .controller("AboutCtrl", ['$scope', '$timeout', '$http', function ($scope, $timeout, $http) {

    $scope.labels =[];
    $scope.data =[[],[]];

    $scope.series =['Books', 'Journals']; 

    function checkVkorg(sorg){
      for (var i = 0; i < $scope.labels.length; i++) {
        if ($scope.labels[i] == sorg) {
          return i;
        };
      };
      return -1;
    }

    $http.get("../../data/convertcsv.json")
    .then(function(response) {
        $scope.res = response.data;

        for (var i = 0; i < 1000 ; i++) {
          var ind  = checkVkorg($scope.res[i].VKORG);
          if ( ind == -1) {
            $scope.labels.push($scope.res[i].VKORG);
            if ($scope.res[i].SPART == '01') {
              $scope.data[0][$scope.data[0].length] = 1; 
              $scope.data[1][$scope.data[1].length] = 0; 
            } else if( $scope.res[i].SPART == '02' ){
              $scope.data[1][$scope.data[1].length] = 1; 
              $scope.data[0][$scope.data[0].length] = 0; 
            };

            
          }else{
            if ($scope.res[i].SPART == '01') {
              $scope.data[0][ind] ++; 
            } else if( $scope.res[i].SPART == '02' ){
              $scope.data[1][ind] ++; 
            }; 
          }; 
           
        };

    });

    $scope.checkDupe = function (item , upd){
      for (var i = 0; i < $scope.positions.length; i++) {
        if ($scope.positions[i].BP == item.BP) {
          if (upd) {
            $scope.positions[i].ORD ++ ;
            $scope.positions[i].QTY += item.QTY ;
          };
          return true;
        };
      };
      return false;
    }

  
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };

 
}])