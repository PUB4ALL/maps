'use strict';

/**
 * @ngdoc function
 * @name threeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the threeApp
 */
angular.module('threeApp')
  .controller('MainCtrl', function (NgMap, $scope, $http) {

   $scope.positions = [];
  $http.get("../../data/convertcsv.json")
    .then(function(response) {
        $scope.data = response.data;

        for (var i = 0, j = 0; i < $scope.data.length; i++) {

          if (!$scope.checkDupe($scope.data[i] , true) ) {

            if ($scope.positions.length < 1000) {
              $scope.positions[j] = $scope.data[i];
              $scope.positions[j].ORD = 1; 
              j++;
            }else{
              return;
            };

          };
           
        };

    });

    $scope.checkDupe = function (item , upd){
      for (var i = 0; i < $scope.positions.length; i++) {
        if ($scope.positions[i].BP == item.BP) {
          if (upd) {
            $scope.positions[i].ORD ++ ;
            $scope.positions[i].QTY += item.QTY ;
          };
          return true;
        };
      };
      return false;
    }

    $scope.modal = UIkit.modal("#item");

    $scope.showDetail = function(e, x){
      $scope.itemData = {};
      $scope.itemData = x;
      $scope.modal.show();  
    } 

    
  });
