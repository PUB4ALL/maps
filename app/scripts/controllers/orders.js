'use strict';

/**
 * @ngdoc function
 * @name threeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the threeApp
 */
angular.module('threeApp')
  .controller("ordersCtrl", ['$scope', '$timeout', '$http', function ($scope, $timeout, $http) {
     $scope.currentPage = 0;
     $scope.pageSize = 10;
     $scope.positions = [];
     // $scope.modal = UIkit.modal.blockUI("loading data ...");

    $http.get("../../data/convertcsv.json")
    .then(function(response) {
        $scope.res = response.data;

        for (var i = 0, j = 0; i < $scope.res.length; i++) {
          $scope.positions[j++] = $scope.res[i]; 
          if (j > 5000) {
            return;
          };
        };

    //     $scope.numberOfPages=function(){
    //     return Math.ceil($scope.data.length/$scope.pageSize);                
    // }

        // $scope.modal.hide();

    });

  

 
}])